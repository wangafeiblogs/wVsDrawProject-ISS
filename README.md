# wVsDrawProject-ISS

#### 介绍
wVsDrawProject项目的bug反馈页面
# OpenClass

#### 软件架构
wVsDrawProject项目的bug反馈页面

- [x] C# 支持完善

支持平台：

- [x] AutoCad2020
- [ ] 浩辰GsCAD
- [ ] 中望ZwCad

#### 介绍
本代码库中的部分代码来自于 OpenClass[[地址](https://gitee.com/wangafeiblogs/open-class)]

## 功能说明
本软件免费使用，每个安装包一个星期自动过期
如需更新，请到QQ群:529711940下载最新版本。
如有需求可联系本人定制开发，QQ:1504187115
本软件包含功能（2022年11月13日）
本软件用途：根据用户提供的桩号进行断面线绘制，根据断面线找到相交的等高线或离散点形成的三角网提取输出cass断面文件，然后根据提取到的自然和设计cass断面文件生成并绘制断面，同步生成断面计算书。
## 一、定桩号

1. 选择一条曲线
2. 指定桩号开始基点
3. 回车 桩号增大
4. 命令结束完成桩号定义
## 二、绘制断面线

1. 选桩号线
2. 设定左右侧宽度
3. 指定布置方式
   1. 单个布置
   2. 批量布置
4. 拖动预览桩号，点击完成布置。
## 三、生成cass文件

1. 选桩号线
2. 选择 样例实体（等高线、离散点、有z值图块）
3. 指定cass文件目录，输入名称确定完成输出
4. 接着选择 另一个样例实体，比如 第一次选择的是 设计线、第二次可以选择 自然线。
## 四、输出断面图，输出计算书

1. 弹出界面，指定设计cass、自然cass文件目录
2. 确定后，会在当前图的坐标原点绘制断面（有图框、有网格、有桩号标记，后期增加设置界面）
3. 同时指定 计算书的输出目录和文件名称
4. 完成 计算出输出后会弹出Excel（电脑上需要安装完整版的Office Excel 否则会报异常）


### 4.根据断面文件生成断面图并计算输出Excel

### 捐赠
如果觉得对你有用，小小支持一下
[支付宝](支付宝.jpeg)
